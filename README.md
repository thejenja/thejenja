<img src="/header.png" width=100%/>

## Introduction

Hi, my name's Evgeniy Khramov. I'm a 15-year-old web programmer and video creator from Orsk. At the moment I'm learning Python and JavaScript.

### Social media

If you want to contact me [go to my website](https://thejenja.github.io/#social).

### Languages:

<p align="left"> <a href="https://www.w3.org/html/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-plain-wordmark.svg" alt="html5" width="40" height="40"/> </a> <a href="https://www.w3schools.com/css/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-plain-wordmark.svg" alt="css3" width="40" height="40"/></a> <a href="https://www.php.net" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/php/php-plain.svg" alt="php" width="40" height="40"/> </a> </p>

### The things I use
<p align="left"><img src="https://img.shields.io/badge/OS-Windows%2011%2021H2-blue?style=for-the-badge&logo=windows">
<img src="https://img.shields.io/badge/OS-Linux%20Mint%2020.2-brightgreen?style=for-the-badge&logo=linuxmint">
<img src="https://img.shields.io/badge/IDE-VSCode-blue?style=for-the-badge&logo=visualstudiocode">
<img src="https://img.shields.io/badge/Browser-Chrome-red?style=for-the-badge&logo=googlechrome"></p>
